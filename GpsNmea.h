#ifndef __GPSNMEA_H__
#define __GPSNMEA_H__

// NMEA sentence structure
// $[SENTENCE ADDRESS],[DATA FIELD]...*[CHECKSUM]\r\n
// Checksum is XOR of all characters between, but not including, the $ and *

#include <Arduino.h>
#include <SoftwareSerial.h>

struct type_date
{
    uint16_t year = 0;
    uint8_t month = 0;
    uint8_t day = 0;
};

struct type_time
{
    uint8_t hour = 0;
    uint8_t minute = 0;
    uint8_t second = 0;
};

// Template for GPS data, with valid flag and updating capable
template <typename T>
class GpsValue
{
public:
    const T &getValue() const { return value; };
    bool isValid() const { return valid; };
    void invalidate() { valid = false; };
    void update();

protected:
    T newValue = {};
    bool newValid = false;

private:
    T value = {};
    bool valid = false;
};

class GpsDate : public GpsValue<type_date>
{
public:
    void parseDate(const String &str);
};

class GpsTime : public GpsValue<type_time>
{
public:
    void parseTime(const String &str);
};

class GpsCoordinate : public GpsValue<double>
{
public:
    void parseDegrees(const String &str);
    void parseDirection(const String &str);
};

class GpsDecimal : public GpsValue<double>
{
public:
    void parseDecimal(const String &str);
};

class GpsInteger : public GpsValue<uint8_t>
{
public:
    void parseInteger(const String &str);
};

// Class for encode GPS NMEA module sentence
class Gps
{
public:
    Gps(uint8_t rxPin, uint8_t txPin);
    void begin();
    // Power control
    void enable();
    void disable();
    // Process data received from GPS module
    bool decode();
    // Data getters
    const GpsInteger &getSatellitesInUse() const { return satellitesInUse; };
    const GpsInteger &getSatellitesInView() const { return satellitesInView; };
    const GpsDate &getDate() const { return date; };
    const GpsTime &getTime() const { return time; };
    const GpsCoordinate &getLatitude() const { return latitude; };
    const GpsCoordinate &getLongitude() const { return longitude; };
    const GpsDecimal &getAltitude() const { return altitude; };
    const GpsDecimal &getHdop() const { return hdop; };

private:
    // Serial link for GPS
    SoftwareSerial gpsSerial;
    // Process field, returns true, when complete sentence decoded
    bool processField();
    // Parsing variables
    enum : uint8_t
    {
        SENTENCE_GGA,
        SENTENCE_RMC,
        SENTENCE_GSV,
        SENTENCE_UNKNOWN
    } sentenceType = SENTENCE_UNKNOWN;
    enum : uint8_t
    {
        FIELD_ADDRESS,
        FIELD_DATA,
        FIELD_CHECKSUM,
        FIELD_UNKNOWN
    } fieldType = FIELD_UNKNOWN;
    uint8_t fieldNumber = 0;
    String field;
    uint8_t sentenceChecksum = 0;
    bool sentenceHasFix = false;
    // Properties
    GpsDate date;
    GpsTime time;
    GpsCoordinate latitude;
    GpsCoordinate longitude;
    GpsDecimal altitude;
    GpsDecimal hdop;
    GpsInteger satellitesInUse;
    GpsInteger satellitesInView;
};

#endif
