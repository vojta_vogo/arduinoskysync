#include "OledSsd1306.h"
#include "Font.h"
#include <Wire.h>

Oled::Oled(uint8_t i2cAddr)
    : oledI2cAddr(i2cAddr)
{
}

void Oled::begin()
{
    // Init I2C
    Wire.begin();
    // Init OLED
    oled.begin(&Adafruit128x64, oledI2cAddr);
    oled.setFont(myFont5x7);
    oled.clear();
}

void Oled::enable()
{
    work = true;
    oled.setContrast(1);
}

void Oled::disable()
{
    work = false;
    oled.setContrast(0);
    row = 0;
}

#define ROW_BUFFER_SIZE 22

void Oled::refresh(const Gps &gps)
{
    if (work)
    {
        // Buffer for one row
        char rowBuffer[ROW_BUFFER_SIZE] = {};
        // Refresh only one row
        switch (row)
        {
        case 0:
            snprintf(rowBuffer, ROW_BUFFER_SIZE, "AUX: %c%c        GPS: %c",
                     auxIn ? 0x80 : '-', auxOut ? 0x81 : '-', gpsIn ? 0x80 : '-');
            // Reset indicators
            auxIn = auxOut = gpsIn = false;
            break;

        case 1:
            snprintf(rowBuffer, ROW_BUFFER_SIZE, "Satellites: %d/%d",
                     gps.getSatellitesInUse().isValid() ? gps.getSatellitesInUse().getValue() : 0,
                     gps.getSatellitesInView().isValid() ? gps.getSatellitesInView().getValue() : 0);
            break;

        case 2:
            if (gps.getDate().isValid())
            {
                snprintf(rowBuffer, ROW_BUFFER_SIZE, "Date UTC: %04d/%02d/%02d",
                         gps.getDate().getValue().year, gps.getDate().getValue().month, gps.getDate().getValue().day);
            }
            else
            {
                strncpy(rowBuffer, "Date UTC: ----/--/--", ROW_BUFFER_SIZE);
            }
            break;

        case 3:
            if (gps.getTime().isValid())
            {
                snprintf(rowBuffer, ROW_BUFFER_SIZE, "Time UTC: %02d:%02d:%02d",
                         gps.getTime().getValue().hour, gps.getTime().getValue().minute, gps.getTime().getValue().second);
            }
            else
            {
                strncpy(rowBuffer, "Time UTC: --:--:--", ROW_BUFFER_SIZE);
            }
            break;

        case 4:
            if (gps.getLatitude().isValid())
            {
                char direction = (gps.getLatitude().getValue() >= 0.0) ? 'N' : 'S';
                double degrees = fabs(gps.getLatitude().getValue());
                double minutes = (degrees - (int)degrees) * 60.0;
                double seconds = (minutes - (int)minutes) * 60.0;
                double secondsHundredths = (seconds - (int)seconds) * 100.0;
                snprintf(rowBuffer, ROW_BUFFER_SIZE, "Lat.: %c %d\x82%02d\x83%02d.%02d\x84",
                         direction, (int)degrees, (int)minutes, (int)seconds, (int)secondsHundredths);
            }
            else
            {
                strncpy(rowBuffer, "Lat: - --\x82--\x83--.--\x84", ROW_BUFFER_SIZE);
            }
            break;

        case 5:
            if (gps.getLongitude().isValid())
            {
                char direction = (gps.getLongitude().getValue() >= 0.0) ? 'E' : 'W';
                double degrees = fabs(gps.getLongitude().getValue());
                double minutes = (degrees - (int)degrees) * 60.0;
                double seconds = (minutes - (int)minutes) * 60.0;
                double secondsHundredths = (seconds - (int)seconds) * 100.0;
                snprintf(rowBuffer, ROW_BUFFER_SIZE, "Lng.: %c %d\x82%02d\x83%02d.%02d\x84",
                         direction, (int)degrees, (int)minutes, (int)seconds, (int)secondsHundredths);
            }
            else
            {
                strncpy(rowBuffer, "Lng.: - ---\x82--\x83--.--\x84", ROW_BUFFER_SIZE);
            }
            break;

        case 6:
            if (gps.getAltitude().isValid())
            {
                double altitude = gps.getAltitude().getValue();
                double altitudeTenths = fabs(altitude - (int)altitude) * 10.0;
                snprintf(rowBuffer, ROW_BUFFER_SIZE, "Altitude: %d.%d m",
                         (int)altitude, (int)altitudeTenths);
            }
            else
            {
                strncpy(rowBuffer, "Altitude: ---.- m", ROW_BUFFER_SIZE);
            }
            break;

        case 7:
            if (gps.getHdop().isValid())
            {
                double accuracy = gps.getHdop().getValue();
                double accuracyTenths = (accuracy - (int)accuracy) * 10.0;
                snprintf(rowBuffer, ROW_BUFFER_SIZE, "Accuracy: %d.%d m",
                         (int)accuracy, (int)accuracyTenths);
            }
            else
            {
                strncpy(rowBuffer, "Accuracy: --.- m", ROW_BUFFER_SIZE);
            }
            break;

        default:
            break;
        }
        // Print row
        oled.setCol(0);
        oled.setRow(row);
        oled.print(rowBuffer);
        // Clear rest of row
        oled.clearToEOL();
        // Increment row
        row = (row + 1) % 8;
    }
}
