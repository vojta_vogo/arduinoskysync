#ifndef __NEXSTAR_H__
#define __NEXSTAR_H__

// Packet structure
// [PREAMBLE][LENGTH][DATA...][CHECKSUM]
// First data byte is source device
// Second data byte is destination device
// Third data byte is message ID
// Other data bytes are message's data
// The checksum is computed by summing the bytes starting from the byte after the preamble and
// ending with the byte before the checksum and taking the LSB of the two's complement

#include <Arduino.h>

class Aux
{
public:
    Aux(uint8_t busyPin);
    void begin();
    // Process data received from AUX
    bool decode();
    // Get last decoded data length
    inline uint8_t getDataLength()
    {
        return dataLength;
    };
    // Get last decoded data
    inline uint8_t getData(byte index)
    {
        return dataBuffer[index];
    };
    // Send a message to AUX
    bool sendData(const uint8_t data[], uint8_t length);

private:
    // Properties
    uint8_t auxBusyPin = 0;
    enum : uint8_t
    {
        WAIT_PREAMBLE,
        WAIT_LENGTH,
        WAIT_DATA,
        WAIT_CHECKSUM
    } state = WAIT_PREAMBLE;
    uint8_t dataLength = 0;
    uint8_t dataBuffer[UINT8_MAX] = {};
    uint8_t dataBufferIndex = 0;
    int32_t dataChecksum = 0;
};

#endif
