/*
  Firmware for open source device based on Arduino and NEO GPS
  compatible with GPS Celestron SkySync for Celestron Nexstar

  Copyright (C) 2023  Vojtěch "vogo" Gondžala

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
  Resources: http://www.paquettefamily.ca/nexstar/NexStar_AUX_Commands_10.pdf
             http://aprs.gids.nl/nmea/

  OLED display library: https://github.com/greiman/SSD1306Ascii
*/

#include "AuxNexStar.h"
#include "GpsNmea.h"
#include "OledSsd1306.h"

// Wiring
#define AUX_BUSY_PIN 2
#define GPS_RX_PIN 4
#define GPS_TX_PIN 5
#define OLED_I2C_ADDRES 0x3c // Usually 0x3c or 0x3d

// GPS messages
#define MSG_GPS_GET_LAT 0x01
#define MSG_GPS_GET_LONG 0x02
#define MSG_GPS_GET_DATE 0x03
#define MSG_GPS_GET_YEAR 0x04
#define MSG_GPS_GET_SAT_INFO 0x07
#define MSG_GPS_GET_TIME 0x33
#define MSG_GPS_TIME_VALID 0x36
#define MSG_GPS_LINKED 0x37
#define MSG_GPS_GET_HW_VER 0x55
#define MSG_GPS_GET_VER 0xfe

// WiFi messages
#define MSG_WIFI_STATUS 0x10

// Interface ID
#define DEV_GPS 0xb0
#define DEV_WIFI 0xb5
#define DEV_DUMMY 0xef

// Hardware version
#define HW_VER 0xaf

// Firmware version
#define FW_VER_MAJOR 1
#define FW_VER_MINOR 0

Aux aux(AUX_BUSY_PIN);
Gps gps(GPS_RX_PIN, GPS_TX_PIN);
Oled oled(OLED_I2C_ADDRES);

// Power save mode one minute after the last relevant GPS message
bool alive = true;
uint32_t aliveMillis = 0;

// Transform coordinate to NexStar format
inline void transformCoordinate(double degrees, uint8_t &b2, uint8_t &b1, uint8_t &b0)
{
    // Coordinate is 24-bit signed integer representing a fraction of 360 degrees
    int32_t l = round((degrees * pow(2, 24)) / 360.0);
    b2 = (l >> 16) & 0xff;
    b1 = (l >> 8) & 0xff;
    b0 = l & 0xff;
}

bool processAux()
{
    if ((aux.getDataLength() > 2) && (aux.getData(1) == DEV_GPS))
    {
        // Set outgoing data
        uint8_t outDataLength = 0;
        uint8_t outData[6] = {};
        // Set header
        outData[0] = DEV_GPS;
        outData[1] = aux.getData(0);
        outData[2] = aux.getData(2);
        // Set message data
        switch (aux.getData(2))
        {
        case MSG_GPS_GET_LAT:
            transformCoordinate(gps.getLatitude().getValue(), outData[3], outData[4], outData[5]);
            outDataLength = 6;
            break;

        case MSG_GPS_GET_LONG:
            transformCoordinate(gps.getLongitude().getValue(), outData[3], outData[4], outData[5]);
            outDataLength = 6;
            break;

        case MSG_GPS_GET_DATE:
            outData[3] = gps.getDate().getValue().month;
            outData[4] = gps.getDate().getValue().day;
            outDataLength = 5;
            break;

        case MSG_GPS_GET_YEAR:
            outData[3] = (gps.getDate().getValue().year >> 8) & 0xff;
            outData[4] = gps.getDate().getValue().year & 0xff;
            outDataLength = 5;
            break;

        case MSG_GPS_GET_SAT_INFO:
            outData[3] = gps.getSatellitesInView().getValue();
            outData[4] = gps.getSatellitesInUse().getValue();
            outDataLength = 5;
            break;

        case MSG_GPS_GET_TIME:
            outData[3] = gps.getTime().getValue().hour;
            outData[4] = gps.getTime().getValue().minute;
            outData[5] = gps.getTime().getValue().second;
            outDataLength = 6;
            break;

        case MSG_GPS_TIME_VALID:
            outData[3] = (gps.getDate().isValid() && gps.getTime().isValid()) ? 0x1 : 0x0;
            outDataLength = 4;
            break;

        case MSG_GPS_LINKED:
            outData[3] = (gps.getDate().isValid() && gps.getTime().isValid() && gps.getLatitude().isValid() && gps.getLongitude().isValid()) ? 0x1 : 0x0;
            outDataLength = 4;
            break;

        case MSG_GPS_GET_HW_VER:
            outData[3] = HW_VER;
            outDataLength = 4;
            break;

        case MSG_GPS_GET_VER:
            outData[3] = FW_VER_MAJOR;
            outData[4] = FW_VER_MINOR;
            outDataLength = 5;
            break;

        default:
            break;
        }
        // Send data
        if (outDataLength && aux.sendData(outData, outDataLength))
        {
            return true;
        }
    }

    return false;
}

void setup()
{
    // Init devices
    aux.begin();
    gps.begin();
    oled.begin();
    // Disable WiFi at startup
    uint8_t data[4] = {DEV_DUMMY, DEV_WIFI, MSG_WIFI_STATUS, 0x00};
    aux.sendData(data, 4);
}

void loop()
{
    // Keep device alive when outgoing data
    bool keepAlive = false;
    // Read AUX input
    if (aux.decode())
    {
        oled.showAuxIn();
        // Process AUX message
        if (processAux())
        {
            oled.showAuxOut();
            keepAlive = true;
        }
    }
    // Read GPS input
    if (gps.decode())
    {
        oled.showGpsIn();
    }
    // Update OLED
    oled.refresh(gps);
    // Maintain power save mode
    if (keepAlive)
    {
        aliveMillis = millis();
        if (!alive)
        {
            alive = true;
            gps.enable();
            oled.enable();
        }
    }
    else if (alive && ((millis() - aliveMillis) > 60000))
    {
        alive = false;
        gps.disable();
        oled.disable();
    }
}
