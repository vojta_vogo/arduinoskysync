# arduinoskysync

Open source device based on Arduino and NEO GPS compatible with GPS Celestron SkySync for Celestron Nexstar

**Components:**
- [Arduino Pro Mini 5V](https://www.laskarduino.cz/arduino-pro-mini--atmega328-5v-16mhz--klon/)
- [GPS Module GY-NEO6MV2](https://www.laskarduino.cz/arduino-gps-modul-gy-neo6mv2/)
- [OLED 0.96" 128x64, I2C](https://www.laskarduino.cz/oled-displej-bily-128x64-0-96--i2c/)
- [Micro Step down](https://www.laskarduino.cz/mikro-step-down-menic--nastavitelny/) - set to 5v
- Resistor 47k
- Diode 1N4148
- Cable with RJ12 connector

**RJ12 default coloring:**

**_But your cable may have a different order!_**
- 1 White
- 2 Black
- 3 Red
- 4 Green
- 5 Yellow
- 6 Blue

**Schematic:**

![](imgs/arduinoskysync.png)

![](imgs/1.jpg)
