#include "GpsNmea.h"

template <typename T>
void GpsValue<T>::update()
{
    valid = newValid;
    newValid = false;

    if (valid)
    {
        value = newValue;
    }
};

// Expected format ddmmyy
void GpsDate::parseDate(const String &str)
{
    if (str.length() == 6)
    {
        newValue.day = str.substring(0, 2).toInt();
        newValue.month = str.substring(2, 4).toInt();
        newValue.year = 2000 + str.substring(4, 6).toInt();
        newValid = (newValue.day > 0) && (newValue.month > 0);
    }
    else
    {
        newValid = false;
    }
}

// Expected format hhmmss or hhmmss.ss
void GpsTime::parseTime(const String &str)
{
    if (str.length() >= 6)
    {
        newValue.hour = str.substring(0, 2).toInt();
        newValue.minute = str.substring(2, 4).toInt();
        newValue.second = str.substring(4, 6).toInt();
        newValid = true;
    }
    else
    {
        newValid = false;
    }
}

// Expected format DDDMM.MMMM
void GpsCoordinate::parseDegrees(const String &str)
{
    if (str.length() > 1)
    {
        double d = str.toDouble();
        newValue = (long)d / 100;
        double minutes = d - (newValue * 100.0);
        newValue += minutes / 60.0;
        newValid = true;
    }
    else
    {
        newValid = false;
    }
}

// Expected one char of N S E W
void GpsCoordinate::parseDirection(const String &str)
{
    if (str.length() == 1)
    {
        switch (str[0])
        {
        case 'N':
        case 'E':
            break;

        case 'S':
        case 'W':
            newValue *= -1.0;
            break;

        default:
            newValid = false;
            break;
        }
    }
    else
    {
        newValid = false;
    }
}

void GpsDecimal::parseDecimal(const String &str)
{
    if (str.length() > 0)
    {
        newValue = str.toDouble();
        newValid = true;
    }
    else
    {
        newValid = true;
    }
}

void GpsInteger::parseInteger(const String &str)
{
    if (str.length() > 0)
    {
        newValue = str.toInt();
        newValid = true;
    }
    else
    {
        newValid = false;
    }
}

Gps::Gps(uint8_t rxPin, uint8_t txPin)
    : gpsSerial(rxPin, txPin)
{
}

void Gps::begin()
{
    // Init serial
    gpsSerial.begin(9600);
}

void Gps::enable()
{
    // Message for enable GPS radio frequency section
    uint8_t gpsRfOn[12] = {0xb5, 0x62, 0x06, 0x04, 0x04, 0x00, 0x00, 0x00, 0x09, 0x00, 0x17, 0x76};
    // Send message to GPS module
    gpsSerial.write(gpsRfOn, 12);
    gpsSerial.flush();
}

void Gps::disable()
{
    // Message for disable GPS radio frequency section
    uint8_t gpsRfOff[12] = {0xb5, 0x62, 0x06, 0x04, 0x04, 0x00, 0x00, 0x00, 0x08, 0x00, 0x16, 0x74};
    // Send message to GPS module
    gpsSerial.write(gpsRfOff, 12);
    gpsSerial.flush();
    // Invalidate all data
    satellitesInUse.invalidate();
    satellitesInView.invalidate();
    date.invalidate();
    time.invalidate();
    latitude.invalidate();
    longitude.invalidate();
    altitude.invalidate();
    hdop.invalidate();
}

bool Gps::decode()
{
    // Return true, when a complete sentence decoded
    bool completeSentence = false;
    // Read data from serial
    while (gpsSerial.available() > 0)
    {
        char c = gpsSerial.read();
        // Char is a terminator, process the field
        if ((c == '$') || (c == ',') || (c == '*') || (c == '\r') || (c == '\n'))
        {
            if (processField())
            {
                completeSentence = true;
            }

            field = "";
        }
        // Eat a char
        switch (c)
        {
        case '$':
            // New sentence
            fieldType = FIELD_ADDRESS;
            fieldNumber = 0;
            sentenceChecksum = 0;
            sentenceHasFix = false;
            break;

        case ',':
            // Data field
            fieldType = FIELD_DATA;
            fieldNumber++;
            sentenceChecksum ^= c;
            break;

        case '*':
            // Checksum field
            fieldType = FIELD_CHECKSUM;
            break;

        case '\r':
        case '\n':
            // End of sentence
            fieldType = FIELD_UNKNOWN;
            break;

        default:
            // Ordinary char, append char to field
            field += c;

            if (fieldType != FIELD_CHECKSUM)
            {
                sentenceChecksum ^= c;
            }
            break;
        }
    }

    return completeSentence;
}

// We need a macro for use sentence and field combination in switch-case
#define COMBINE(a, b) ((uint16_t)(((uint8_t)a) | (((uint16_t)((uint8_t)b)) << 8)))

bool Gps::processField()
{
    switch (fieldType)
    {
    case FIELD_ADDRESS:
        // Set sentence type
        if (field == "GPGGA")
        {
            sentenceType = SENTENCE_GGA;
        }
        else if (field == "GPRMC")
        {
            sentenceType = SENTENCE_RMC;
        }
        else if (field == "GPGSV")
        {
            sentenceType = SENTENCE_GSV;
        }
        else
        {
            sentenceType = SENTENCE_UNKNOWN;
        }
        break;

    case FIELD_DATA:
        // Set data by sentence and field
        switch (COMBINE(sentenceType, fieldNumber))
        {
        case COMBINE(SENTENCE_GGA, 1):
        case COMBINE(SENTENCE_RMC, 1):
            time.parseTime(field);
            break;

        case COMBINE(SENTENCE_GGA, 2):
        case COMBINE(SENTENCE_RMC, 3):
            latitude.parseDegrees(field);
            break;

        case COMBINE(SENTENCE_GGA, 3):
        case COMBINE(SENTENCE_RMC, 4):
            latitude.parseDirection(field);
            break;

        case COMBINE(SENTENCE_GGA, 4):
        case COMBINE(SENTENCE_RMC, 5):
            longitude.parseDegrees(field);
            break;

        case COMBINE(SENTENCE_GGA, 5):
        case COMBINE(SENTENCE_RMC, 6):
            longitude.parseDirection(field);
            break;

        case COMBINE(SENTENCE_GGA, 6):
            sentenceHasFix = field.toInt() > 0;
            break;

        case COMBINE(SENTENCE_GGA, 7):
            satellitesInUse.parseInteger(field);
            break;

        case COMBINE(SENTENCE_GGA, 8):
            hdop.parseDecimal(field);
            break;

        case COMBINE(SENTENCE_GGA, 9):
            altitude.parseDecimal(field);
            break;

        case COMBINE(SENTENCE_RMC, 9):
            date.parseDate(field);
            break;

        case COMBINE(SENTENCE_RMC, 2):
            sentenceHasFix = field[0] == 'A';
            break;

        case COMBINE(SENTENCE_GSV, 3):
            satellitesInView.parseInteger(field);
            break;

        default:
            break;
        }
        break;

    case FIELD_CHECKSUM:
        // Test checksum
        if (sentenceChecksum == strtol(field.c_str(), NULL, 16))
        {
            // Update data by sentence
            switch (sentenceType)
            {
            case SENTENCE_GGA:
                time.update();
                satellitesInUse.update();

                if (sentenceHasFix)
                {
                    latitude.update();
                    longitude.update();
                    hdop.update();
                    altitude.update();
                }
                else
                {
                    latitude.invalidate();
                    longitude.invalidate();
                    hdop.invalidate();
                    altitude.invalidate();
                }
                break;

            case SENTENCE_RMC:
                time.update();
                date.update();

                if (sentenceHasFix)
                {
                    latitude.update();
                    longitude.update();
                }
                else
                {
                    latitude.invalidate();
                    longitude.invalidate();
                }
                break;

            case SENTENCE_GSV:
                satellitesInView.update();
                break;

            default:
                break;
            }
            // Checksum OK, complete sentence received
            return true;
        }
        break;

    default:
        break;
    }
    // Sentence is incomplete
    return false;
}
