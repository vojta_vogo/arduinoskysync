#ifndef __OLEDSSD1306_H__
#define __OLEDSSD1306_H__

#include "GpsNmea.h"
#include <Arduino.h>
#include <SSD1306AsciiWire.h>

class Oled
{
public:
    Oled(byte i2cAddr);
    void begin();
    // enable OLED
    void enable();
    // disable OLED
    void disable();
    // Show GPS data
    void refresh(const Gps &gps);
    // Comunication indicators
    inline void showAuxIn() { auxIn = true; };
    inline void showAuxOut() { auxOut = true; };
    inline void showGpsIn() { gpsIn = true; };

private:
    // OLED object
    SSD1306AsciiWire oled;
    // OLED I2C address
    uint8_t oledI2cAddr = 0;
    // Comunication indicators
    bool auxIn = false;
    bool auxOut = false;
    bool gpsIn = false;
    // Refreshed row
    uint8_t row = 0;
    // Working flag, enable/disable OLED
    bool work = true;
};

#endif
