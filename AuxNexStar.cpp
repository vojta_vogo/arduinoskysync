#include "AuxNexStar.h"

#define auxSerial Serial
#define AUX_RX_PIN 0
#define AUX_TX_PIN 1
#define AUX_PREAMBLE 0x3b

Aux::Aux(uint8_t busyPin)
    : auxBusyPin(busyPin)
{
}

void Aux::begin()
{
    // Init serial
    auxSerial.begin(19200, SERIAL_8N2);
    pinMode(AUX_RX_PIN, INPUT); // Default PULLUP is to much for shared bus
    pinMode(auxBusyPin, INPUT_PULLUP);
}

bool Aux::decode()
{
    // Process incoming data
    while (auxSerial.available() > 0)
    {
        uint8_t b = auxSerial.read();
        switch (state)
        {
        default:
        case WAIT_PREAMBLE:
            if (b == AUX_PREAMBLE)
            {
                // Preamble received, wait for length
                state = WAIT_LENGTH;
                dataLength = 0;
                dataBufferIndex = 0;
                dataChecksum = 0;
            }
            break;

        case WAIT_LENGTH:
            // Length received wait for data
            state = WAIT_DATA;
            dataLength = b;
            dataChecksum += b;
            break;

        case WAIT_DATA:
            // Insert data
            dataBuffer[dataBufferIndex++] = b;
            dataChecksum += b;
            // Length reached, wait for checksum
            if (dataBufferIndex >= dataLength)
            {
                state = WAIT_CHECKSUM;
            }
            break;

        case WAIT_CHECKSUM:
            // Checksum received, wait for next packet preamble
            state = WAIT_PREAMBLE;
            // Test checksum
            return ((uint8_t)((-dataChecksum) & 0xff)) == b;
            break;
        }
    }

    return false;
}

bool Aux::sendData(const uint8_t data[], uint8_t length)
{
    // Wait until AUX is not busy
    for (unsigned long millis0 = millis(); digitalRead(auxBusyPin) == LOW; delayMicroseconds(500))
    {
        if ((millis() - millis0) > 1000)
        {
            return false;
        }
    }
    // Claim AUX bus
    pinMode(auxBusyPin, OUTPUT);
    digitalWrite(auxBusyPin, LOW);
    delayMicroseconds(100);
    // Send packet
    auxSerial.write(AUX_PREAMBLE);
    auxSerial.write(length);
    // Calculate checksum
    int32_t sum = length;
    // Data
    for (uint8_t i = 0; i < length; i++)
    {
        auxSerial.write(data[i]);
        sum += data[i];
    }
    // Checksum
    auxSerial.write((-sum) & 0xff);
    // Clear AUX
    auxSerial.flush();
    pinMode(auxBusyPin, INPUT_PULLUP);
    // Message sent
    return true;
}
